/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.actions;

import io.undertow.Version;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Objects;
import org.seppiko.commons.utils.StreamUtil;
import org.seppiko.commons.utils.http.HttpStatus;
import org.seppiko.commons.utils.http.MediaType;
import org.seppiko.leaf.gateway.models.ResponseEntity;
import org.seppiko.leaf.gateway.utils.JsonUtil;

/**
 * @author Leonard Woo
 */
public abstract class AbstractAction extends HttpServlet {

  protected abstract ResponseEntity ContentHandleExecution(HttpServletRequest request) throws ServletException, IOException;

  protected ResponseEntity methodNotAllowed() {
    LinkedHashMap<String, Object> jsonMap = new LinkedHashMap<>();
    jsonMap.put("code", 405);
    jsonMap.put("message", "Method Not Allowed.");
    return sendJson(HttpStatus.METHOD_NOT_ALLOWED, jsonMap);
  }

  protected ResponseEntity badRequest(String msg) {
    LinkedHashMap<String, Object> jsonMap = new LinkedHashMap<>();
    jsonMap.put("code", 400);
    jsonMap.put("message", msg);
    return sendJson(HttpStatus.BAD_REQUEST, jsonMap);
  }

  protected ResponseEntity sendJson(HttpStatus status, LinkedHashMap<String, Object> jsonMap) {
    return new ResponseEntity(status, MediaType.APPLICATION_JSON,
        JsonUtil.toJson(jsonMap).getBytes(StandardCharsets.UTF_8));
  }

  protected byte[] getBody(InputStream in, int totalLen) throws IOException {
    BufferedReader breader = StreamUtil.loadReader(in);
    ByteBuffer bb = ByteBuffer.allocate(totalLen);
    while(breader.ready()) {
      bb.put((byte) breader.read());
    }
    return bb.array();
  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    ResponseEntity chxResp = ContentHandleExecution(req);
    if (Objects.isNull(chxResp)) {
      throw new ServletException("Action methods is null");
    }
    resp.setStatus(chxResp.status().getCode());
    resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
    resp.addHeader("server", "Undertow/" + Version.getVersionString());
    resp.setContentType(chxResp.type().getValue());
    byte[] body = chxResp.body();
    resp.setContentLength(body.length);
    OutputStream os = resp.getOutputStream();
    os.write(body);
    os.flush();
    os.close();
  }
}
