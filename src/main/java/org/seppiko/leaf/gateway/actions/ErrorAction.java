/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.actions;

import io.undertow.server.HttpServerExchange;
import io.undertow.servlet.api.ExceptionHandler;
import io.undertow.util.StatusCodes;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import org.seppiko.commons.utils.http.MediaType;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.gateway.utils.JsonUtil;

/**
 * Error page
 *
 * @author Leonard Woo
 */
public class ErrorAction implements ExceptionHandler {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  public ExceptionHandler get() {
    return this;
  }

  private String service() {
    LinkedHashMap<String, Object> respJsonObject = new LinkedHashMap<>();
    respJsonObject.put("code", 500);
    respJsonObject.put("message", "SERVER ERROR");
    return JsonUtil.toJson(respJsonObject);
  }

  @Override
  public boolean handleThrowable(HttpServerExchange httpServerExchange,
      ServletRequest servletRequest, ServletResponse servletResponse, Throwable throwable) {
    try {
      httpServerExchange.setStatusCode(StatusCodes.INTERNAL_SERVER_ERROR);
      servletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
      servletResponse.setContentType(MediaType.APPLICATION_JSON.getValue());
      byte[] body = service().getBytes(StandardCharsets.UTF_8);
      servletResponse.setContentLength(body.length);
      OutputStream os = servletResponse.getOutputStream();
      os.write(body);
      os.flush();
      os.close();
      logger.warn("WARNING: SERVICE OR REMOTE SERVER ERROR!!! " + throwable.getMessage());
    } catch (IOException | RuntimeException e) {
      logger.error("Failed to return exception message." + e.getMessage());
      return false;
    }
    return true;
  }
}
