/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.actions;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.LinkedHashMap;
import org.seppiko.commons.utils.http.HttpMethod;
import org.seppiko.commons.utils.http.HttpStatus;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.gateway.config.LGWConfiguration;
import org.seppiko.leaf.gateway.models.ResponseEntity;
import org.seppiko.leaf.gateway.services.LeafService;

/**
 * Leaf Gateway page
 *
 * @author Leonard Woo
 */
@WebServlet
public class IndexAction extends AbstractAction {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final LeafService service = LeafService.getInstance();

  private final String sIDName = LGWConfiguration.getInstance().getSIDname();

  @Override
  protected ResponseEntity ContentHandleExecution(HttpServletRequest request)
      throws ServletException, IOException {
    if (!request.getMethod().equals(HttpMethod.GET.name())) {
      return methodNotAllowed();
    }
    String reqSID = request.getParameter(sIDName);
    if (reqSID != null) {
      try {
        LinkedHashMap<String, Object> respJson =
            new LinkedHashMap<>(service.randomMode(sIDName, reqSID));

        return sendJson(HttpStatus.OK, respJson);
      } catch (InterruptedException ex) {
        logger.warn("Service error", ex);
      }
    }
    return badRequest("Request error");
  }

}
