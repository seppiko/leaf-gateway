/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.utils;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ExceptionHandler;
import io.undertow.servlet.api.FilterInfo;
import io.undertow.servlet.api.ListenerInfo;
import io.undertow.servlet.api.ServletInfo;
import jakarta.servlet.ServletException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import org.seppiko.leaf.gateway.config.LGWConfiguration;
import org.xnio.Options;

/**
 * Undertow Util
 *
 * @author Leonard Woo
 */
public class UndertowUtil {

  private static final String DEPLOYMENT_NAME =
      LGWConfiguration.getInstance().getArtifactId() + ".war";
  private String rootPath = "/";
  private ExceptionHandler exceptionHandler;

  public void setRootPath(String rootPath) {
    this.rootPath = rootPath;
  }

  public void setExceptionHandler(ExceptionHandler exceptionHandler) {
    this.exceptionHandler = exceptionHandler;
  }

  public void start(InetSocketAddress bind, ArrayList<ServletInfo> actions)
          throws ServletException, RuntimeException {
    start(bind, null, actions);
  }

  public void start(InetSocketAddress bind, ArrayList<FilterInfo> filters, ArrayList<ServletInfo> actions)
          throws ServletException, RuntimeException {
    start(bind, filters, null, actions);
  }

  public void start(InetSocketAddress bind, ArrayList<FilterInfo> filters, ArrayList<ListenerInfo> listeners,
                           ArrayList<ServletInfo> actions)
          throws ServletException, RuntimeException {
    DeploymentInfo servletBuilder = Servlets.deployment()
            .setClassLoader(UndertowUtil.class.getClassLoader())
            .setContextPath(rootPath)
            .setDeploymentName(DEPLOYMENT_NAME)
            .setExceptionHandler(exceptionHandler);

    if (actions != null && actions.size() > 0) {
      servletBuilder.addServlets(actions);
    } else {
      throw new ServletException("Not Found Actions");
    }

    if (filters != null && filters.size() > 0) {
      servletBuilder.addFilters(filters);
    }

    if (listeners != null && listeners.size() > 0) {
      servletBuilder.addListeners(listeners);
    }

    DeploymentManager manager = Servlets.defaultContainer()
            .addDeployment(servletBuilder);
    manager.deploy();

    PathHandler path = Handlers.path()
        .addPrefixPath(rootPath, manager.start());

    Undertow server = Undertow.builder()
            .addHttpListener(bind.getPort(), bind.getHostName())
            .setHandler(path)
            .setWorkerOption(Options.KEEP_ALIVE, false)
                .setBufferSize(1024)
            .build();

    server.start();
  }

}
