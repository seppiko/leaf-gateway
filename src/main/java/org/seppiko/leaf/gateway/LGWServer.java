/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletInfo;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.gateway.actions.ErrorAction;
import org.seppiko.leaf.gateway.actions.IndexAction;
import org.seppiko.leaf.gateway.config.Environment;
import org.seppiko.leaf.gateway.config.LGWConfiguration;
import org.seppiko.leaf.gateway.utils.UndertowUtil;


/**
 * Leaf Gateway server
 *
 * @author Leonard Woo
 */
public class LGWServer {

  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger("LWG");
    try {
      String host = Environment.LEAF_DEFAULT_HOST;
      String port = System.getProperty(Environment.DEFAULT_SERVER_PORT_CONFIG, Environment.LEAF_GATEWAY_DEFAULT_PORT);

      LGWConfiguration configuration = LGWConfiguration.getInstance();
      if(configuration.isEmpty()) {
        throw new ExceptionInInitializerError("Configuration file initialization failed.");
      }

      configuration.printBanner(System.out);

      ArrayList<ServletInfo> servlets = new ArrayList<>();
      servlets.add(Servlets.servlet("index", IndexAction.class).addMapping("/"));

      InetSocketAddress bind = new InetSocketAddress(host, Integer.parseInt(port));
      UndertowUtil undertow = new UndertowUtil();
      undertow.setExceptionHandler(new ErrorAction().get());
      undertow.start(bind, servlets);
      logger.info("Started server on port " + port + " (http)");

    } catch (Throwable ex) {
      logger.error("SERVER ERROR!!!", ex);
    }
  }
}
