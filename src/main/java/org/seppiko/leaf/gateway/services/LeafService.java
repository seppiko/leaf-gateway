/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Random;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.gateway.models.TargetEntity;
import org.seppiko.leaf.gateway.config.LGWConfiguration;
import org.seppiko.leaf.gateway.utils.JsonUtil;

/**
 * Leaf gRPC service
 *
 * @author Leonard Woo
 */
public class LeafService {

  private static LeafService INSTANCE = new LeafService();

  public static LeafService getInstance() {
    return INSTANCE;
  }

  private LeafService() {
  }

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final ArrayList<TargetEntity> targets = LGWConfiguration.getInstance().getTargets();

  public LinkedHashMap<String, Object> randomMode(String sIDName, String reqSID)
      throws InterruptedException {
    LinkedHashMap<String, Object> respJson = new LinkedHashMap<>();

    int choose = (targets.size() > 1)? (new Random().nextInt(targets.size())):0;
    TargetEntity target = targets.get(choose);
    long leafId = leafCaller(target);
    respJson.put(sIDName, reqSID);
    respJson.put("leafId", leafId + "");

    logger.info("target: " + target + ", request id: " + reqSID);

    return respJson;
  }

  private long leafCaller(TargetEntity target) throws InterruptedException {
    LeafClient client = new LeafClient(target.host(), target.port());
    try {
      client.setLeaf(target.serviceId(), target.nodeId());
      return client.getId();
    } finally {
      client.shutdown();
    }
  }
}
