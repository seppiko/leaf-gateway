/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.services;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.concurrent.TimeUnit;
import org.seppiko.leaf.proto.LeafMsg;
import org.seppiko.leaf.proto.LeafReq;
import org.seppiko.leaf.proto.LeafResp;
import org.seppiko.leaf.proto.LeafServiceGrpc;

/**
 * Leaf Client
 *
 * @author Leonard Woo
 */
public class LeafClient {

  private final ManagedChannel channel;
  private final LeafServiceGrpc.LeafServiceBlockingStub blockingStub;

  private int serviceId;
  private int nodeId;

  public LeafClient(String host, int port) {
    this( ManagedChannelBuilder.forAddress(host, port)
        .usePlaintext() );
  }

  LeafClient(ManagedChannelBuilder<?> channelBuilder) {
    this.channel = channelBuilder.build();
    this.blockingStub = LeafServiceGrpc.newBlockingStub(this.channel);
  }

  public void setLeaf(int serviceId, int nodeId) {
    this.serviceId = serviceId;
    this.nodeId = nodeId;
  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  public long getId(){
    LeafReq req = LeafReq.newBuilder().setServiceId(serviceId).setNodeId(nodeId).build();
    LeafResp resp = blockingStub.idGen(req);
    return resp.getId();
  }

  public String formatId(long id){
    LeafResp req = LeafResp.newBuilder().setId(id).build();
    LeafMsg resp = blockingStub.idPar(req);
    return resp.getMessage();
  }
}
