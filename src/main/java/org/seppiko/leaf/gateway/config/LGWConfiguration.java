/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.leaf.gateway.config;

import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Objects;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.seppiko.commons.utils.StreamUtil;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.leaf.gateway.models.TargetEntity;
import org.seppiko.leaf.gateway.utils.JsonUtil;


/**
 * Leaf Gateway Configuration
 *
 * @author Leonard Woo
 */
public class LGWConfiguration {

  private static LGWConfiguration instance = new LGWConfiguration();
  public static LGWConfiguration getInstance() {
    return instance;
  }
  private LGWConfiguration() {
    init();
  }

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private ArrayList<TargetEntity> targets;

  public ArrayList<TargetEntity> getTargets() {
    return targets;
  }

  private String sIDname;

  public String getSIDname() {
    return sIDname;
  }

  private void init() {
    try {
      String path = System.getProperty(Environment.LEAF_GATEWAT_CONFIG_FILE, Environment.LEAF_GATEWAY_DEFAULT_FILENAME);
      InputStream in = StreamUtil.findFileInputStream(this.getClass(), path);
      if (Objects.isNull(in)) {
        throw new FileNotFoundException(path + " loaded failed.");
      }
      BufferedReader reader = StreamUtil.loadReader(in);

      JsonObject root = JsonUtil.fromJson(reader).getAsJsonObject();
      targets = JsonUtil.fromJsonArray(root.get("leaves").getAsJsonArray(), TargetEntity.class);
      logger.info("Remote Leaves Configuration: " + JsonUtil.toJson(targets));

      sIDname = root.get("sidname").getAsString();
      logger.info("Source ID name:" + sIDname);

    } catch (Exception e) {
      logger.error("Load config file failed.", e);
      targets = null;
    }
  }

  public boolean isEmpty() {
    return targets == null || targets.size() < 1;
  }

  private final String groupId = "org.seppiko";
  private final String artifactId = "leaf-gateway";

  public String getArtifactId() {
    return artifactId;
  }

  private static final String[] BANNER = {
    "",
    "  ____                   _ _           _                __    ____       _                           ",
    " / ___|  ___ _ __  _ __ (_) | _____   | |    ___  __ _ / _|  / ___| __ _| |_ _____      ____ _ _   _ ",
    " \\___ \\ / _ \\ '_ \\| '_ \\| | |/ / _ \\  | |   / _ \\/ _` | |_  | |  _ / _` | __/ _ \\ \\ /\\ / / _` | | | |",
    "  ___) |  __/ |_) | |_) | |   < (_) | | |__|  __/ (_| |  _| | |_| | (_| | ||  __/\\ V  V / (_| | |_| |",
    " |____/ \\___| .__/| .__/|_|_|\\_\\___/  |_____\\___|\\__,_|_|    \\____|\\__,_|\\__\\___| \\_/\\_/ \\__,_|\\__, |",
    "            |_|   |_|                                                                          |___/ "
  };
  private String project_name = ":: Seppiko Leaf Gateway ::";

  public void printBanner(PrintStream out) {
    try {
      Model model = new MavenXpp3Reader().read(new InputStreamReader(getPomInputStream()));
      String version = model.getVersion();
      String prefix = "(";
      String suffix = ")";

      version = (version != null) ? prefix + version + suffix : "";
      int maxLength = 0;
      for (String s : BANNER) {
        maxLength = Math.max(maxLength, s.length());
        out.println(s);
      }

      StringBuilder padding = new StringBuilder();
      while (padding.length() < (maxLength - (version.length() + project_name.length())) ) {
        padding.append(" ");
      }
      out.println(project_name + padding.toString() + version);
      out.println();
      out.flush();
    } catch (Exception ex) {
      logger.warn("BANNER load failed.", ex);
    }
  }

  private InputStream getPomInputStream() throws FileNotFoundException {
    InputStream is = this.getClass().getResourceAsStream(
        "/META-INF/maven/" + groupId + "/" + artifactId + "/pom.xml");
    if(is == null) {
      is = new FileInputStream("pom.xml");
    }
    return is;
  }

}
