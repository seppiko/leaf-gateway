# Seppiko Leaf Gateway

![GitLab](https://img.shields.io/gitlab/license/seppiko/leaf-gateway?color=blue&style=flat-square)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/seppiko/leaf-gateway?style=flat-square)

Seppiko Leaf Gateway is [Seppiko Leaf](https://gitlab.com/seppiko/leaf) RESTful API.
And provide load balancing.

## Compile and package

Copy Seppiko Leaf `/src/main/proto/leaf.proto` to `/src/main/proto`.
```
mvn clean
mvn protobuf:compile
mvn protobuf:compile-custom
mvn package
```

## Configuration

`leafgateway.json` MUST BE a JSON array.
If just have one config,random choose is not enable.

## How to use

You need to modify `leafgateway.json` for configure leaf server.

Use `-Dlgw.configFile=leafgateway.json` custom `leafgateway.json` file path.

Use `-Dserver.port` for change listener port.

Access `http://SERVER_ADDRESS:SERVER_PORT/?srcId=<id>`,that `<id>` will be return.

## Response

```
{
  "srcId":"<id> parameter provided when you visit",
  "leafId":"leaf server return id"
}
```

Please check `srcId` is equal on cluster mode.

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
